const User = require('../models/User')
const jwt = require('jsonwebtoken');

//handle error
const handleErrors=(err)=>{
  console.log('CATCH ERROR \n');
  console.log(err.message, err.code);
  let errors = { email: '', password: '' };

  //incorrect email
  if(err.message === 'incorrect email'){
    errors.email = 'that email is not registered';
  }
  //incorrect password
  if(err.message === 'incorrect password'){
    errors.password = 'that password is not valid';
  }

  //duplicate error code
  if(err.code === 11000){
    errors.email = 'that email is already register';
    return errors;
  }

  //validation errors
  if(err.message.includes('user validation failed')){
    //console.log(Object.values(err.errors));
    Object.values(err.errors).forEach(({properties}) => {
      console.log(properties);
      errors[properties.path] = properties.message;
    });
  }

  return errors;
}

//3 days in seconde
const maxAge = 2*24*60*60;
//token
const createToken = (id) => {
  return jwt.sign({id},'secretcode',{
    expiresIn:maxAge
  });
}

module.exports.signup_get = (req,res) => {
  res.render('signup');
};

module.exports.login_get = (req,res) => {
  res.render('login');
};

module.exports.signup_post = async (req,res) => {
  const { email, password, nom, prenom, classe, utag} = req.body;
  //console.log(email, password);

  //new user
  try{
    console.log('try')
    const user = await User.create({email, password, nom, prenom, classe, utag});
    const token = createToken(user._id);
    res.cookie('jwt', token,{ httpOnly: true, maxAge: maxAge*1000});
    res.status(201).json({user: user._id});
  }
  catch(err){
    const errors = handleErrors(err);
    res.status(400).json({errors});
  }
};

module.exports.login_post = async (req,res) => {
  //console.log(req.body);
  const { email,password} = req.body;
  console.log(email, password);

  try{
    const user = await User.login(email, password);
    const token = createToken(user._id);
    res.cookie('jwt', token,{ httpOnly: true, maxAge: maxAge*1000});
    res.status(200).json({user: user._id});
  }catch(err){
    const errors = handleErrors(err);
    res.status(400).json({errors});
  } 

};

//deconnexion
module.exports.logout_get = async (req,res) => {
 //supprimer les cookies de connexion = le remplacer par un "blank cookie" sans fonction 
  res.cookie('jwt', '', {maxAge: 1});
  res.redirect('/');

};

//get data users
module.exports.users_get = async(req,res) => {
  // get request
  try{
    const users = await User.find();
    //  res.json(users);
     res.render('users', { data: users});
  }catch(err){
    res.json(err);
  }
}

// get specific user data
module.exports.user_get = async(req,res) => {
  // get request
  try{
    const user = await User.findById(req.params.userId);
    res.json(user);
  }catch(err){
    res.json(err);
  }
}
