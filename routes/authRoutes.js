const { Router } = require('express');
const authController = require('../controllers/authController')
//new instence of router
const router = Router();

//callback fonction fire in authController
router.get('/signup', authController.signup_get);

router.post('/signup', authController.signup_post);

router.get('/login', authController.login_get);

router.post('/login', authController.login_post);

router.get('/logout', authController.logout_get);

router.get('/users', authController.users_get);

router.get('/users/:userId', authController.user_get);

module.exports = router;