const jwt = require('jsonwebtoken');
const User = require('../models/User');


//this fonction allow acces on certain page if u are login( thx to token jwt)
const requireAuth = (req,res, next) => {
  const token = req.cookies.jwt;
  //check json web token existed & if he is verified
  if(token){
    jwt.verify(token, 'secretcode', (err, decodedToken)=> {
      if(err){
        console.log(err.message);
        res.redirect('/login');
      }else{
        console.log(decodedToken);
        next();
      }
    })
  }else{
    res.redirect('/login');
  }
}

//check current user
const checkuser = (req,res,next) => {
  const token = req.cookies.jwt;
  if(token){
    jwt.verify(token, 'secretcode', async (err, decodedToken)=> {
      if(err){
        console.log(err.message);
        res.locals.user = null;
        next();
      }else{
        console.log(decodedToken);
        let user = await User.findById(decodedToken.id);
        res.locals.user = user;
        next();
      }
    });
  }else{
    res.locals.user = null;
    next();
  }
}

module.exports = { requireAuth, checkuser };