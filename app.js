// all required express and mongoose + serialPort
const SerialPort = require("serialport");
const express = require('express');
const mongoose = require('mongoose');
const authRoutes = require('./routes/authRoutes');
const cookieParser = require('cookie-parser');
const { truncate } = require('lodash');
const { requireAuth, checkuser } = require('./middleware/authMiddleware');
const User = require('./models/User');
const axios = require('axios');


//express app
const app = express();
// ajout de socket.io
const server = require('http').Server(app)
const io = require('socket.io')(server)

//middleWare
app.use(express.static('public'));
//parse Json in a JS object
app.use(express.json());
//cookie
app.use(cookieParser());

//view engine
app.set('view engine', 'ejs');

//db Connection
const dbURI = 'mongodb+srv://me:test123@cluster0.zkeyn.mongodb.net/logger?retryWrites=true&w=majority';
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true})
  .then((result) => {
    console.log("Connected to DB");
    console.log("go to -> http://localhost:3000");
    server.listen(3000);
  })
  .catch((err)=>{
    console.log("Connection failed")
    console.log(err);
  })

  //serial communication
const parsers = SerialPort.parsers;
const parser = new parsers.Readline({
  delimiter: '\r\n',
  autoOpen: true
});

var arduinoCOMPort = "COM5";
const port = new SerialPort(arduinoCOMPort, {  
  baudRate: 9600,
  autoOpen: true
 });
port.pipe(parser);
parser.on('open', function(){
  console.log('connexion is opened');
});

port.on('open', function(){
  console.log('connexion is opened');
});

// port.write("Alexandre", function(err, results) {
//   if (err) {
//           return console.log('Error on write: ', err.message);
//         }else{
//           console.log('message writen');
//         }
// });  

// setting socket connection 
io.on('connection', (socket) => {
  console.log("user connected: " + socket.id);
})

parser.on('data', function(data){
  console.log("Data read : ["+data+"]");
  io.emit('data', data);
  port.write(data);
});

//routes
//renvoie la chaine html quand la req a une res
app.get('*',checkuser);//apply checkUser middleWare on every get request
app.get('/',(req,res) => {
  //User.find({}, function(err,pers){
  //res.render('home',{pers: pers})
  //})
  res.render('home')
});
app.get('/controlPanel', requireAuth, (req,res) => {
  res.render('controlPanel')
});

//go INFO.txt
app.use(authRoutes);
