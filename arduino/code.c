#include <SPI.h> // SPI
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <MFRC522.h> // RFID

#define SS_PIN 9
#define RST_PIN 10

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

int led = 8;
// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

// Déclaration 
MFRC522 rfid(SS_PIN, RST_PIN); 

// Tableau contentent l'ID
byte nuidPICC[4];

String uTagString = "";
String uTagRead = "";
String testId = "179DF63A";

// Initialization of the OLED screen, RFID module and blink the LED to confirm start-up.
void setup() 
{ 
  //We blink the LED for startup
  pinMode(led, OUTPUT);
  digitalWrite(led, 1);
  delay(1000);
  digitalWrite(led, 0);

  // Init RS232
  Serial.begin(9600);

  // Init SPI bus
  SPI.begin(); 

  // Init MFRC522 
  rfid.PCD_Init(); 

//Wa allocate the SSd1306 module and display an error message if otherwise
   if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  display.setTextSize(1);
  display.setTextColor(WHITE);
  //row //column
  display.setCursor(0,0);
  /*
 for(int i=0;i<SCREEN_WIDTH;i++){
    for(int j=0;j<SCREEN_HEIGHT;j++){
      display.setCursor(i,j);
       display.print("■");
    }
  }

  display.display();*/
}

//tone with different fraq if the tag is not registered
void ledBuzzerWrong(){
  digitalWrite(led, 1);
  tone(7, 100, 70);
  delay(50);
  digitalWrite(led, 0);
  delay(50);
  digitalWrite(led, 1);
  tone(7, 100, 70);
  delay(200);
  digitalWrite(led, 0);
}
//tone with different fraq if the tag is registered
void ledBuzzer(){
  digitalWrite(led, 1);
  tone(7, 700, 70);
  delay(50);
  digitalWrite(led, 0);
  delay(50);
  digitalWrite(led, 1);
  tone(7, 700, 70);
  delay(200);
  digitalWrite(led, 0);
}

//We read port RX for incoming data from the computer
String receiveData() {
     String inputString = "";         // a String to hold incoming data
     while(Serial.available()) {
     inputString = Serial.readString();// read the incoming data as string
}
// We display the corresponding string on the OLED screen
  display.setCursor(1, 40);
  display.print(inputString);

  return inputString;
}

//Function to convert an Hex into a String 
String stringifyTag(byte nuidPICC[4]){
  String utag = "";
  String cast = "";
  for (byte i = 0; i < 4; i++) 
  {
    utag += String(nuidPICC[i], HEX);
  }
  Serial.println();
   //The string is then set in Uppercase.
  utag.toUpperCase();
  return utag;
}

//Function to compare the ID tag with an existing person from the Database.
int compareData(String uTagString,String uTagRead){
  display.clearDisplay();
  if(uTagString.equals(uTagRead)){
    if(uTagString == "16D06135"){
      display.setCursor(1, 40);
      display.print("Alexandre Sebire");
      ledBuzzer();
    }
    if(uTagString == "179DF63A"){
      display.setCursor(1, 40);
      display.print("Maxime Schneider");
      ledBuzzer();
    }
    if(uTagString == "B66269AF"){
      display.setCursor(1, 40);
      display.print("Adrien Kreutz");
      ledBuzzer();
    }
    if(uTagString == "4513B81B"){
      display.setCursor(1, 40);
      display.print("Roxane Calor");
      ledBuzzer();
    }
    if(uTagString == "FDC44B13"){
      display.setCursor(1, 40);
      display.print("Jess Escalade");
      ledBuzzer();
    }
   /*  display.setCursor(1, 40);
    display.print(uTagString);
    display.setCursor(1, 50);
    display.print(uTagRead);
    display.setCursor(40, 55);
    display.print("samba");*/
  }else{
    display.setCursor(1, 40);
    display.print("Not registered");
    ledBuzzerWrong();
  }
}



 
void loop() 
{
 // Loop initialization if no tag are scanned
  if ( !rfid.PICC_IsNewCardPresent())
    return;

 //Check if a tag is scanned
  if ( !rfid.PICC_ReadCardSerial())
    return;

  //Save of the UID RFID tag (4 octets) 
  for (byte i = 0; i < 4; i++) 
  {
    nuidPICC[i] = rfid.uid.uidByte[i];
  }
 
  uTagRead=receiveData();
  uTagString=stringifyTag(nuidPICC);
  compareData(uTagString,uTagRead);

  // Affichage de l'ID sur OLED
  display.setCursor(1, 10);
  display.print("UID TAG is : ");
    display.setCursor(1, 20);
  for (byte i = 0; i < 4; i++) 
  {
    Serial.print(nuidPICC[i], HEX);
    //Serial.print(" ");
     display.print(nuidPICC[i], HEX);
     display.print(" ");
     display.display();
  }
  Serial.println();
  display.setCursor(0, 10);
  display.println();
  //beep and light
  //ledBuzzer();
  display.clearDisplay();

  // Re-Init RFID
  rfid.PICC_HaltA(); // Halt PICC
  rfid.PCD_StopCrypto1(); // Stop encryption on PCD
}